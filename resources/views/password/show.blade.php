@extends ('layouts.app')

@section('content')
    <h1 class="text-center display-4">Your secret is:</h1>
    <div class="row align-items-center justify-content-center">
        <div class="col-xs col-xl-4 col-lg-6 col-md-8">
            <div class="form-group">
                    <textarea name="secret" class="form-control" rows="3" placeholder="Your secret"
                              autofocus>{{ $secret }}</textarea>
            </div>
        </div>
    </div>
@endsection
