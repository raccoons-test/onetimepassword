@extends('layouts.app')

@section('content')
    <h1 class="text-center display-4">Securely store your passwords</h1>
    <div class="row align-items-center justify-content-center">
        <div class="col-xs col-xl-4 col-lg-6 col-md-8">
            <form action="/" method="post" class="mt-5">
                @csrf
                <div class="form-group">
                    <textarea name="secret" class="form-control @error('secret') is-invalid @enderror" rows="3"
                              placeholder="Your secret" autofocus></textarea>
                    @error('secret')
                    <div class="invalid-feedback">{{ $errors->first('secret') }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Expiration time</label>
                    <select class="form-control @error('time') is-invalid @enderror" name="time">
                        <option value="60">1 minute</option>
                        <option value="300">5 minutes</option>
                        <option value="1800">30 minutes</option>
                        <option value="3600">1 hour</option>
                    </select>
                    @error('time')
                    <div class="invalid-feedback">{{ $errors->first('time') }}</div>
                    @enderror
                </div>
                @isset($link)
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Your link</span>
                            </div>
                            <input type="text" class="form-control" autofocus placeholder="{{ $link }}"
                                   value="{{ $link }}" />
                        </div>
                    </div>
                @endisset
                <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Store</button>
            </form>
        </div>
    </div>
@endsection
