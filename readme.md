One Time Password
===

This service is intended to securely transfer your passwords.

Install
---

1. Clone this repository
2. Install dependencies via composer `composer install` or `php composer.phar install` (More: https://getcomposer.org/doc/00-intro.md)

Configuration
---

First you need to generate your application key via `php artisan key:generate` (More: https://laravel.com/docs/6.x/installation#configuration).
Alternatively you can set this value in `APP_KEY` environment variable.

Next setup Redis database connection in `./config/database.php`, or use environment variables instead.

Run
---

You are free to use any environment to run this application.

I'd suggest that you will use laradock in the following way: `docker-compose up -d nginx redis`

More information about how to install and use laradock
you could find here: https://laradock.io/introduction/#quick-overview 

**NOTE**: application entry point is located under `./public` directory
or `./server.php`
