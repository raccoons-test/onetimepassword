<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Http\Requests\StorePassword;
use Illuminate\Support\Facades\Redis;

/**
 * Class PasswordController
 *
 * @package App\Http\Controllers
 */
class PasswordController extends Controller
{
    /**
     * Stores password.
     *
     * @param StorePassword $request
     * @return mixed
     */
    public function store(StorePassword $request)
    {
        $id = Str::random(32);
        $time = (int)$request->input('time');
        $secret = encrypt($request->input('secret'));
        Redis::setEx($id, $time, $secret);

        return view('password.store', ['link' => url("/$id")]);
    }

    /**
     * Shows password by the given id.
     *
     * @param string $id
     * @return mixed
     */
    public function show($id)
    {
        $value = Redis::get($id);
        if (!$value) {
            abort(404);
        }
        Redis::del($id);
        $secret = decrypt($value);

        return view('password.show', ['secret' => $secret]);
    }
}
