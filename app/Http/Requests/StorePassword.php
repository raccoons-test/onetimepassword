<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StorePassword
 *
 * @package App\Http\Requests
 */
class StorePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'secret' => ['required', 'string', 'max:255'],
            'time' => ['required', 'integer', 'min:60', 'max:3600']
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            /* --- secret --- */
            'secret.required' => 'This field is required.',
            'secret.string' => 'The secret must be a string.',
            'secret.max' => 'The secret must be 255 symbols maximum.',
            /* --- time --- */
            'time.required' => 'This field is required',
            'time.integer' => 'This field must be integer value.'
        ];
    }
}
